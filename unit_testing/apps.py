from django.apps import AppConfig


class UnitTestingConfig(AppConfig):
    name = 'unit_testing'

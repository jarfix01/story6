from django.db import models
from django.forms import ModelForm

# Create your models here.
class Test_Input(models.Model):
    input_text = models.CharField(max_length=200)

    def __str__(self):
        return self.input_text
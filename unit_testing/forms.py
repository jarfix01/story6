from django import forms
from .models import Test_Input

class InputForm(forms.ModelForm):
    class Meta:
        model = Test_Input
        fields = '__all__'
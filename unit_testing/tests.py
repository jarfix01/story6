from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.utils import timezone
from django.http import HttpRequest
from unit_testing.views import ShowStatus, add
from .views import ShowStatus
from .models import Test_Input
from .forms import InputForm
# Create your tests here.

class TestUrls(TestCase):

    def test_url_exists(self):
        response = Client().get('/status', follow=True)
        self.assertEqual(response.status_code,200)

    def test_using_index(self):
        found = resolve ('/status/')
        self.assertEqual(found.func, ShowStatus)

class TestModels(TestCase):

    def test_input_creation(self):
        Test_Input.objects.create(input_text='abi ganteng')
        ins = Test_Input.objects.all().count()
        self.assertEqual(ins,1)


class TestViews(TestCase):
    
    def test_landing_page_is_not_empty(self):
        self.assertIsNotNone("index.html")

    def test_landing_page_is_found(self):
        url = reverse ("status")
        resp = self.client.get(url)

        self.assertEqual(resp.status_code,200)

    def test_landing_page_is_found1(self):
        url = reverse ("add")
        resp = self.client.get(url)

        self.assertEqual(resp.status_code,200)
    

class TestHTML(TestCase):

    def test_template_used(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'unit_testing/index.html')

    def test_content_in_landing_page(self):
        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, how are you?', html_response)
        self.assertIn('<table>',html_response)
        self.assertTrue(html_response.startswith('<!DOCTYPE html>'))



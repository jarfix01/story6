from django.urls import path
from . import views

urlpatterns=[
    path ('status/', views.ShowStatus, name='status'),
    path ('add/', views.add, name='add')
]
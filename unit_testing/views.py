from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Test_Input
from .forms import InputForm


# Create your views here.
def ShowStatus(request):
    context = {
        'inputs':Test_Input.objects.all()
    }
    return render (request, 'unit_testing/index.html', context)

def add(request):
    if request.method == 'POST':
        form = InputForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('add')

    else:
        context = {'inputs' : Test_Input.objects.all()}
        return render (request, 'unit_testing/index.html', context)
